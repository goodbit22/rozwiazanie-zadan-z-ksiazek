#!/usr/bin/bash
#Napisz polecenie sed kopiujące plik do standardowego wyjścia i usuwające wszystkie wiersze
#rozpoczynające się od słowa Dzisiaj.
echo "zad1"
sed -n '/^Dzisiaj/ !p ' ex1_2.txt

#Napisz polecenie sed kopiujące do standardowego wyjścia tylko te wiersze pliku, które
#rozpoczynają się od słowa Dzisiaj.
echo "zad2"
sed -n '/^Dzisiaj/ p ' ex1_2.txt
