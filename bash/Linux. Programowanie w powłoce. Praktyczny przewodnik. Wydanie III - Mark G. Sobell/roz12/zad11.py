"""
Utwórz listę zawierającą cztery elementy. Wykonaj kopię listy, a następnie w kopii zmień jeden
z elementów. Pokaż, że ten sam element w oryginalnej liście nie został zmieniony
"""
lista = [1,2,3,4]
lista2 = lista[:]
lista2[1] = 3
print("zwykla lista = ",lista)
print("druga lista = ",lista2)
