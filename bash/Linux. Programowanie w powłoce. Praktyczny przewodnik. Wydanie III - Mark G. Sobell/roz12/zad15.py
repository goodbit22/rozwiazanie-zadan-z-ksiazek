"""
Napisz funkcję pobierającą liczbę całkowitą jako jej argument val. Funkcja powinna poprosić
użytkownika o podanie liczby. Jeżeli liczba będzie większa niż val, funkcja ma wyświetlić
komunikat Zbyt duża i zwrócić wartość 1. Jeżeli liczba będzie mniejsza niż val, funkcja ma
wyświetlić komunikat Zbyt mała i zwrócić wartość -1. Jeżeli liczba będzie równa val, funkcja
ma wyświetlić komunikat Trafiłeś! i zwrócić wartość 0. Wywołuj funkcję tak długo, dopóki
użytkownik nie wprowadzi właściwej liczby.
"""
import random
def func(val):
    user_val = int(input("podaj liczbe: "))
    if user_val == val:
        print("trafiles")
        return 0
    elif user_val < val:
        print("za mala liczba")
        return -1
    else:
        print("za duza liczba")
        return 1
w = random.randint(3,9)
ret = 1
while ret != 0:
    ret = func(w)
    if ret == 0:
        break
