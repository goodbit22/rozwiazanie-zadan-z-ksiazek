"""
Przepisz ćwiczenie 15., aby program wywoływał funkcję wraz z losowo wybraną liczbą
z zakresu od 0 do 10 włącznie. (Podpowiedź: funkcja randint() z biblioteki random
zwraca losowo wybraną liczbę z zakresu wskazywanego przez jej dwa argumenty)
"""
import random
def func(val):
    user_val = int(input("podaj liczbe: "))
    if user_val == val:
        print("trafiles")
        return 0
    elif user_val < val:
        print("za mala liczba")
        return -1
    else:
        print("za duza liczba")
        return 1
w = random.randint(3,9)
ret = 1
while ret != 0:
    ret = func(w)
    if ret == 0:
        break

