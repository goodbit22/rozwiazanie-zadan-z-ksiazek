"""
Używając interaktywnej powłoki Pythona, wykorzystaj pętlę for do przeprowadzenia iteracji
przez elementy listy utworzonej w ćwiczeniu 3. oraz wyświetl skróty nazw miesięcy z kropką
na końcu, po jednym skrócie w każdym wierszu. (Podpowiedź: kropka jest ciągiem tekstowym).
"""
months = ["St", "Lut", "Marz", "Kw", "Maj", "Czer"]
for x in months: 
    print(x + ".")
