"""
Napisz funkcję zliczającą wszystkie znaki oraz samogłoski w ciągu tekstowym wprowadzonym
przez użytkownika. Następnie napisz procedurę wywołującą utworzoną wcześniej funkcję
i wyświetlającą przedstawione poniżej dane wyjściowe:
$ ./count_all.py
Podaj dowolny tekst: Słońce wschodzi na wschodzie i zachodzi na zachodzie.
17 liter w 53 znakach to samogłoski.
"""

def len_text(x):
    sam = "AEIOUYaeiouy"
    il = 0
    for l in x:
        for s in  sam:
            if l == s: 
                il += 1
    return [len(x), il]

def wyj():
    x = input("Podaj dowolny tekst: ")
    print("Ciąg tekstowy",x,"ma",len_text(x)[1],"samoglosek")
    print("Ciąg tekstowy ", x , " ma  " , len_text(x)[0] ," znaków długości")
wyj()

