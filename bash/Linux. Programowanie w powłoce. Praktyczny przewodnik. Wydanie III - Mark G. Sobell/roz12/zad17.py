"""
Napisz funkcję zliczającą znaki w ciągu tekstowym wprowadzonym przez użytkownika.
Następnie utwórz procedurę wywołującą tę funkcję i wyświetlającą następujące dane
wyjściowe:
$ ./count_letters.py
Podaj dowolny tekst: W Polsce trwa zima
Ciąg tekstowy "W Polsce trwa zima " ma  18 znaków długości.
"""
def len_text(x):
    return len(x)
def wys():
    x = input("podaj tekst: ")
    print("Ciąg tekstowy ", x , " ma  " , len_text(x) ," znaków długości")
wys()


