"""
Utwórz słownik, w którym kluczami będą nazwy miesięcy trzeciego kwartału roku, a wartościami
liczby dni w poszczególnych miesiącach. Wyświetl słownik, klucze i wartości. Do słownika dodaj
dziesiąty miesiąc roku i wyświetl jedynie liczbę dni w tym miesiącu.
"""
sl = {'Lipiec':31, "Sierpien":30, "Wrzesien":31}
print("slownik: ",sl)

for key in sl.keys():
    print("klucze: ", key, "wartosc: ", sl[key])

sl['paz'] = 30
print(sl['paz'])
