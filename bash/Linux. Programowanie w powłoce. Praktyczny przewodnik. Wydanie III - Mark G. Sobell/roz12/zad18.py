"""
Napisz funkcję zliczającą samogłoski w ciągu tekstowym wprowadzonym przez użytkownika.
Upewnij się o zliczeniu samogłosek zapisanych małymi i wielkimi literami. Następnie napisz
procedurę wywołującą utworzoną wcześniej funkcję i wyświetlającą przedstawione poniżej
dane wyjściowe:
$ ./count_vowels.py
Podaj dowolny tekst: Idź naprzód, młody człowieku!
Ciąg tekstowy "Idź naprzód, młody człowieku!" ma 7 samogłosek
"""
def ilosc_sam(x):
    sam = "AEIOUYaeiouy"
    il = 0
    for l in x:
        for s in  sam:
            if l == x: 
                il += 1
    return il

def wyj():
    x = input("Podaj dowolny tekst: ")
    print("Ciąg tekstowy",x,"ma",ilosc_sam(x),"samoglosek")

wyj()
