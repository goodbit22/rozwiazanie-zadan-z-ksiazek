"""
Zdefiniuj funkcję o nazwie cents2(), która zwraca dokładną wartość argumentu podzielonego
przez 100 (to znaczy wraz z częścią dziesiętną, o ile taka istnieje). Upewnij się, że funkcja nie
skraca wartości otrzymanej w wyniku dzielenia, na przykład:
cents2(12345)
123.45
"""
def cents2(x):
    return x / 100
print(cents2(12345))
