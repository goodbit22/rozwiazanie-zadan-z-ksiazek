#!/bin/bash

function www(){
if [ $# -eq 0  ];then
	echo "nie podales zadnego argumentu"
	exit 2

elif [ $# -eq 1 ];then
		 length=0
		if [  -d "$1" ];then
				echo "jest taki katalog" 
				cd "$1" || echo "nie da sie przejsc"
				for i in *
				do
						if [ -f "$i" ];then
							if [ "$length" -lt "${#i}" ];then
								length="${#i}"
							fi
						fi
				done
				echo "$length"
		else
				echo "nie ma takiego katalogu"
				exit 3		
		fi
else
	echo "" 
fi

}
www "$1"
