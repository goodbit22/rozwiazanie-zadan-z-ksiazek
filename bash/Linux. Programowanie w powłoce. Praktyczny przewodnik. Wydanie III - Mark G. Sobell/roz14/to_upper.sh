#!/bin/bash
#set -x
while read arg
do
   echo "$arg" | tr '[a-z]' '[A-Z]'
done
