1. Stwórz dwa hosty wirtualne. Jeden o nazwie “pierwszy.pl”, a drugi o nazwie
“drugi.pl”. Pod każdym umieść oddzielny plik index.html tak, aby dało się
zorientować, który host został wywołany. Za każdym razem, gdy ktoś odpyta o
nieistniejący host (np. ‘trzeci.pl’), otwierać ma się zawartość ‘drugi.pl’.
Podpowiedź: site-enabled + site-available + domyślny vhost (jak się go
ustawia?)
2. Spraw, aby domyślnym plikiem startowym strony ‘drugi.pl’ był ‘start.html’ zamiast
‘index.html’. Dla wszystkich pozostałych domen plikiem startowym ma być
‘cokolwiek.html’.
Podpowiedź: dyrektywa ‘DirectoryIndex’ na poziomie vhosta + rekonfiguracja
modułu ‘dir’
3. Spraw, aby otworzenie adresu “drugi.pl/abc” powodowało przekierowanie
użytkownika na stronę mikr.us.
Podpowiedź: aktywny mod_rewrite + odpowiednia reguła
4. Spraw, aby serwer do każdej swojej odpowiedzi doklejał nagłówek ‘X-Test:
siema!’.
Podpowiedź: aktywny mod_headers + odpowiednia konfiguracja
5. Spraw, aby Twój Apache przedstawiał się jako Nginx w nagłówku ‘server’.
Podpowiedź: mod_header
6. Zabroń użytkownikom pobierania plików z rozszerzeniem JPG w ramach hosta
‘drugi.pl’ (pozostałe mają działać).
Podpowiedź: FilesMatch + operator ‘require all ...’ lub ‘deny from ...’
7. Serwer ‘pierwszy.pl’ ma obsługiwać wszystkie możliwe subdomeny w ramach
swojej domeny, czyli np: ‘test01.pierwszy.pl’, ‘test02.pierwszy.pl’, czy nawet
‘losowy-ciag-znakow.pierwszy.pl’.
Podpowiedź: ServerName + ServerAlias
8. Zabroń ludziom z adresów IP 123.123.*.* wchodzić na stronę ‘drugi.pl’.
Podpowiedź: ‘Deny from ...’ połączone z Location lub Directory w ramach
vhosta
9. Spraw, aby każde odwołanie do nieistniejącego pliku (error 404) wyświetlało
komunikat “Ojeeej... pliku nie ma!”.
Podpowiedź: ErrorDocument + ścieżka do pliku z komunikatem lub sam
komunikat
10. Twój Apache przy domyślnej konfiguracji, prawdopodobnie na starcie uruchamia
5 swoich procesów. Spraw, aby na starcie uruchomił ich 10, ale w razie dużego
ruchu nigdy nie uruchamiał więcej niż 20 instancji.
Podpowiedź: konfiguracja MPM (moduły)
11. Sprawdź, jak długo uruchomiony jest Apache, ale korzystając jedynie z narzędzia
CURL. Metoda ta zwróci Ci wiele innych, ciekawych statystyk, ale działa tylko na
Twoim serwerze (nie da się jej użyć ‘z internet