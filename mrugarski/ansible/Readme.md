1. Wykonaj polecenie ‘uptime’ na wszystkich serwerach z grupy WEB z wyjątkiem
web03. Zrób to jednym poleceniem, bez tworzenia playbooka
Podpowiedź: wywołanie ad-hoc. Moduł command (domyślny) lub shell. Trzeba
użyć wykluczeń w ‘limit’
2. Na serwerze ‘mysql’ odblokuj na firewallu (ufw) ruch na port 3306 ze wszystkich
serwerów z grupy WEB
Podpowiedź: wymagane jest stworzenie playbooka z pętlą. Adresy IP hostów
pobierzesz pluginem ‘lookup’ z parametrem ‘dig’. Moduł do zarządzania UFW
nazywa się po prostu ‘ufw’.
3. Załóż katalog “klucze” i wrzuć do niego trzy publiczne klucze SSH (każdy w
osobnym pliku). Każdy z nich dodaj do authorized_keys na wszystkich serwerach
z inventory
Podpowiedź: moduł authorized_keys + pętla po serwerach + pętla po kluczach
4. Na każdym serwerze zainstaluj fail2ban. Skonfiguruj go tak, aby nie banował
żadnego serwerów z inventory, czyli skonfiguruj odpowiednio linijkę ‘ignoreip’ w
pliku jail.conf. Po zmianach przeładuj fail2ban, ale spraw aby reload nie oznaczał
statusu serwera jako ‘changed’
Podpowiedź: dwa taski (zmiana configi i reload) + moduł lineinfile + sklejanie
nazw serwerów w jednego stringa przez modyfikator ‘join’ + definicja
when_changed przy zadaniu z reload
5. Przygotuj playbooka gotowego do obsłużenia zarówno Ubuntu jak i systemów z
rodziny RedHat. Zainstaluj na każdym serwerze paczkę ‘vim’. Dla serwerów z
rodziny Debiana użyj ‘apt’, a dla tych z rodziny RedHata użyj ‘jum’
Podpowiedź: dodaj argument ‘when’ i sprawdzaj wartość ‘os_family’ w tzw.
faktach
6. Załóż na każdym serwerze plik /tmp/host_XYZ, gdzie XYZ to nazwa domenowa
danego serwera. Jako zawartość pliku wstaw tekst ‘OK’. Jeśli w katalogu /tmp/
istnieją inne pliki z prefiksem ‘host_’, usuń je.
Podpowiedź: moduł copy + definicja content + usuwanie wg wzorca (dowolną
metodą). Pamiętaj tylko, aby sprzątać przed tworzeniem pliku, a nie po.
7. Na każdym z hostów postaw kontener dockera z Nginx, wystawiając jego port 80
na zewnątrz.
Podpowiedź: użyj roli ‘docker’ (znajdziesz ją w Ansible Galaxy). Po drodze
trzeba będzie zadbać o zależności w Pythonie (instalacja modułu ‘docker’ przez
pip)
8. Upewnij się, że na każdym serwerze istnieje grupa ‘wheel’ (jeśli jej nie ma, to
dodaj) i że należy do niej użytkownik ‘stefan’ (jeśli go nie ma, to załóż). Spraw,
aby użytkownicy z grupę wheel mieli prawo używania polecenia ‘su'
Podpowiedź: moduły user, group, lineinwhile. Musisz zmodyfikować plik /etc/
sudoers
9. Napisz playbooka, który postawi Wordpressa na aktualnym serwerze dla domeny
testuje.pl
Podpowiedź: musisz zainstalować Apache + PHP + MySQL i najprościej będzie
posłużyć się narzędziem ‘wpcli’, które za Ciebie ściągnie Wordpressa, rozpakuje
go, skonfiguruje i uruchomi instalacjęgrant

10. Spróbuj przebudować playbooka z zadania numer 9 w taki sposób, aby nie
wykorzystywać żadnego wbudowanego modułu Ansible. Wykorzystaj gotowe
‘role’ do postawienia wcześniej wspomnianej konfiguracji
Podpowiedź: gotowe role i opis ich użycia i instalacji znajdziesz na Ansible
Galaxy
11. Przygotuj playbooka do zabezpieczenia serwera SSH. Wyłącz możliwość
bezpośredniego logowania się na konto ‘root’. Zmień domyślny numer portu
usługi z 22 na 10022. Skonfiguruj firewalla (UFW) w taki sposób, aby akceptował
połączenia na nowy numer portu i odrzucał na stary. Spraw, aby logowanie było
możliwe jedynie za pomocą klucza (wyłącz dostęp po haśle). Postaraj
się wszystkie zmiany w pliku kon figuracyjnym SSH wprowadzić w jednej pętli
(bez wywoływania tylu pasków ile z
