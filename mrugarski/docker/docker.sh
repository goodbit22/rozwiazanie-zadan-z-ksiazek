#!/bin/bash
#zad1
docker run --rm -p 80:80 --memory="256m" -it \
-v "$(pwd)/config/default.conf:/etc/nginx/default.conf" \
-v "$(pwd)/config/default.conf:/etc/nginx/conf.d/default.conf" \
-v "$(pwd)/strona:/tmp/strona" nginx:latest 

#zad2 
docker save nginx:latest | gzip > nginx.tgz
docker load < nginx.tgz

#zad3 
docker build -f Dockerfile_ex3 -t nginx_obraz_moje:1.0 .

#zad4
docker-compose -f docker-compose_ex4.yaml up -d

#zad6 
docker build -f Dockerfile.ex6 -t application:1.0 .  
docker run application:1.0

#zad7
openssl req -x509 -nodes -newkey rsa:4096 -keyout nginx_key.pem -out nginx_cert.pem -sha512 -days 365 \
    -subj "/C=PL/ST=Warsaw/L=Warsaw/O=Al/OU=IT Department/CN=localhost"
docker-compose -f docker-compose_ex7.yaml up -d
cp nginx_key.pem letsencrypt/keys
cp nginx_cert.pem letsencrypt/csr

#zad8 
docker run -d --name ex8 nginx:latest  
docker cp test.txt ex8:test.txt 
docker exec -it ex8 sh 

#zad9
docker build -f Dockerfile.ex9 -t apache-goodbit:1.0 .  

#zad10
curl --unix-socket /var/run/docker.sock http://localhost/v1.41/images/json | jq 