#!/bin/bash 

# zad1
touch raport.txt && chmod 640 raport.txt && chown Marian:uzytkownicy raport.txt

# zad2
chmod 664 logi.txt & chown Marian:uzytkownicy  logi.txt && chattr +a logi.txt

# zad3
chattr +i config.yaml

# zad4
chmod 1777 /tmp/dane

# zad5
touch pierwszy.txt && ln pierwszy.txt drugi.txt

# zad6
dd bs=1000000 count=100 if=/dev/zero of=testowy

# zad7
touch -d '12 June 1990' starocie.txt

# zad8
sudo apt-get install rename && rename 's/.txt/.doc' *.doc 

# zad9
find ./  -name "b*"  -type f -size +1M  -exec rm "{}" \+  

# zad10
find ./ ! -name "*x" -type f   -exec rm "{}" \+  

# zad11
tar -czf paczka.tar.gz  $(find ./  -name "*.txt"  -type f -size +1M)

find ./  -name "*.txt"  -type f -size +1M | xargs tar -czf paczka.tar.gz
 