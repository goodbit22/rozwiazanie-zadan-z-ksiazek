1. Załóż jednym poleceniem (nie interaktywnie!) użytkownika o nazwie ‘Marian’.
Spraw, aby jego główną grupą użytkownika była ‘zarzad‘, a grupy pomocnicze to
‘adm’ oraz ‘audio’. Marian ma mieć powłokę ustawioną na /bin/sh.
Podpowiedź: man useradd
2. Spraw, aby każdy nowo założony użytkownik w systemie miał w swoim katalogu
plik ‘regulamin.txt’ z jakimś krótkim tekstem w środku.
Podpowiedź: odpowiedni parametr do useradd + katalog tzw. szkieletu (skel)
3. Zmień użytkownikowi Marian powłokę na /usr/bin/nano (to nie jest poprawny
shell! Musisz to jakoś obejść). Gdy Ci się uda, zmień mu powłokę na /bin/bash,
aby dało się na tym użytkowniku nadal pracować.
Podpowiedź: narzędzie do zmiany shella (jedno polecenie) + lista dozwolonych
powłok w /etc/
4. Ustaw użytkownikowi ‘Marian’ hasło ‘banany69’, ale w sposób nieinteraktywny,
za pomocą jednego polecenia.
Podpowiedź: echo + polecenie do zmiany hasła (ale nie passwd, bo ono jest
interaktywne!).
5. Wykonaj polecenie ‘whoami’ jako Marian, ale będąc zalogowanym jako root.
Podpowiedź: sudo
6. Zmień (jednym poleceniem) podstawową grupę użytkownika ‘Marian’ na
‘pracownicy’. Jeśli jest taka potrzeba, to przy okazji utwórz taką grupę.
7. Wymuś na użytkowniku ‘Marian’ regularną zmianę hasła co 7 dni (jednym
poleceniem).
Podpowiedź: chage
8. Zablokuj konto ‘Marian’ tak, aby nie dało się na niego zalogować (nie chodzi o
zmianę hasła, ani usunięcie konta), a później ponownie odblokuj tego
użytkownika.
Podpowiedź: passwd lub chage
9. Spraw, aby nowo zakładani użytkownicy mieli automatycznie przypisywane
numery ID od 8000 w górę.
Podpowiedź: /etc/login.defs
10. Spraw, aby Marian po zalogowaniu się na swoje konto zobaczył aktualny uptime i
load na serwerze (zakładamy, że Marian używa basha)
Podpowiedź: polecenie uptime + pliki ~/.bash_profile lub ~/.bashrc (poczytaj o
różnicach!).
11. Spraw, aby użytkownik ‘Marian’ mógł uruchomić maksymalnie 5 procesów
jednocześnie.