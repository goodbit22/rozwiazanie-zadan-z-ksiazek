1. Utwórz bazę danych o nazwie customers
use customers

2. Utwórz kolekcje: clients, orders, departments, products
db.createCollection('clients')
db.createCollection('orders')
db.createCollection('departments')
db.createCollection('products')

3. Usuń kolekcję o nazwie products
db.products.drop()

4. Dodaj dokumenty z materiałów do kolekcji.
db.customer.insertMany([{
    "_id" : 1,
    "firstName" : "Adam",
    "lastName" : "Kowalski",
    "phone" : "235 786 123",
    "email" : "adam_kowalski@inlook.pl",
    "created_date" : "2021-01-16",
    "purchase" : ["o2", "o4", "o6"]
},
{
    "_id" : 2,
    "firstName" : "Jan",
    "lastName" : "Kowalski",
    "phone" : "111 760 760",
    "email" : "jan_kowalski@rmail.pl",
    "created_date" : "2021-02-16",
    "purchase" : ["o8", "o9", "o10", "o11", "o12"]
},
{
    "_id" : 3,
    "firstName" : "Ewa",
    "lastName" : "Nowak",
    "phone" : "768 211 546",
    "email" : "ewanowak@poczta.eu",
    "created_date" : "2021-02-18",
    "isBusiness" : false,
    "purchase" : ["o1"]
},
{
    "_id" : 4,
    "firstName" : "Dominika",
    "lastName" : "Prosta",
    "email" : "prosta@pigeon.net",
    "created_date" : "2021-03-21",
    "purchase" : ["o5", "o7"]
},
{
    "_id" : 5,
    "firstName" : "Antoni",
    "lastName" : "Pit",
    "phone" : "122 506 506",
    "email" : "antoni.pit@firma.eu",
    "created_date" : "2021-04-30",
    "isBusiness" : true,
    "NIP" : "1256917711",
    "purchase" : ["o13"]
},
{
    "_id" : 6,
    "firstName" : "Agnieszka",
    "lastName" : "Zaradna",
    "phone" : "100 700 600",
    "email" : "a.zaradnapit@biznes.eu",
    "created_date" : "2021-05-06",
    "isBusiness" : true,
    "NIP" : "5348001235",
    "purchase" : ["o14", "o15", "o16", "o17", "o18"]
},
{
    "firstName" : "Patryk",
    "lastName" : "Bez",
    "created_date" : "2021-07-07"
}
])

mongoimport --uri='mongodb://mongoadmin:admin@localhost:27017/?authSource=admin'  -c departments --file  Departments/department06.json
mongoimport --uri='mongodb://mongoadmin:admin@localhost:27017/?authSource=admin'  -c departments --file  Departments/department05.json
mongoimport --uri='mongodb://mongoadmin:admin@localhost:27017/?authSource=admin'  -c departments --file  Departments/department04.json
mongoimport --uri='mongodb://mongoadmin:admin@localhost:27017/?authSource=admin'  -c departments --file  Departments/department03.json
mongoimport --uri='mongodb://mongoadmin:admin@localhost:27017/?authSource=admin'  -c departments --file  Departments/department02.json
mongoimport --uri='mongodb://mongoadmin:admin@localhost:27017/?authSource=admin'  -c departments --file  Departments/department01.json

db.orders.insertMany(
[
{
    "_id" : "o1",
    "customer" : 1,
    "quantity" : 10,
    "cost" : 1200.00,
    "discount" : 20,
    "totalCost" : 960.00
},
{
    "_id" : "o2",
    "customer" : 1,
    "product" : "Papier do drukarki x5000",
    "cost" : 220.00,
    "totalCost" : 220.00
},
{
    "_id" : "o3",
    "customer" : 1,
    "quantity" : 2,
    "cost" : 58.00,
    "totalCost" : 58.00
},
{
    "_id" : "o4",
    "customer" : 2,
    "quantity" : 5,
    "product" : "Tusz czarny do drukarki",
    "cost" : 500.00,
    "discount" : 10,
    "totalCost" : 450.00
}
])


5. Wyświetl wszystkie dokumenty kolekcji orders
db.orders.find())

6. Wyświetl tylko jeden dokument z kolekcji clients
db.clients.findOne()

7. Wyświetl dokładnie trzy dokumenty z kolekcji departments
db.departments.find().limit(3)

8. Wyświetl klucze: _id, cost, totalCost z dokumentów kolekcji orders
db.orders.find({},{"_id":1, "cost": 1, "totalCost": 1 })

9. Wyświetl wszystkie klucze poza kluczem product z dokumentów znajdujących się w kolekcji
clients.
db.clients.find({},{"product": 0})

10.Wyświetl dokumenty z kolekcji orders, posortowane po kluczu customer w sposób malejący,
a po totalCost w sposób rosnący.
db.orders.find().sort({"totalCost": 1, "customer": -1})

11.Wyświetl dokumenty z orders, gdzie totalCost przekracza 10000.
db.orders.find({"totalCost": {'$gt': 10000}})

12.Wyświetl dokumenty z departments, gdzie city to: Poznań, Warszarwa albo Łódź.
db.departments.find({"city": {"$in": ['Poznan', 'Warszawa', 'Lódz']}})

13.Wyświetl dokumenty z clients, gdzie firstName to Jan, a lastName to Nowak.
db.clients.find({"firstName": "Jan","lastName": "Nowak"})
db.clients.find({$and: [{'firstname':'Jan'}, {'lastName':'Nowak'}]})

14.Wyświetl tylko zamówienia z kolekcji orders, gdzie discount jest mniejsze niż 10 lub
cost większy niż 5000. Posortuj po kluczu cost w sposób malejący.
db.orders.find( {$or: [ {"discount": {'$lt': 10 }}, {'cost': {$gt: 5000}}  ] }).sort({"cost":  -1})

15.Zaktualizuj dokument jeden dokument (dowolny) w kolekcji clients, dodając nowy klucz o
nazwie isValid z wartością true. 
db.clients.updateOne({"isTrue": false},{$set: {"isValid": true}})

16.Zaktualizuj wszystkie dokumenty w kolekcji departments o klucz status z wartością A,
które są w Warszawie. Dodatkowo utwórz nowy dokument.
db.departments.updateMany({"city": "Warszawa"}, {$set: {"status": 'A'}}, {upsert: true} )

17.Usuń dokumenty z kolekcji orders, które należą do klienta o ID = 5.
db.orders.deleteMany({"customer": 5})

18.Wykonaj grupowanie po kluczu discount dokumentów z kolekcji orders i wyświetl średnią
wartość cost dla każdej grupy.
db.orders.aggregate( {"$group": {_id: "discount", "avg": {$avg: "$cost"}}})


19.Utwórz indeksy w kolekcji clients: jeden o nazwie klienci na kluczach firstName i
lastName w sposób rosnący, drugi na kluczu phone w sposób malejący.
db.clients.createIndex({'firstName': 1, "lastName": 1}, {name:"index_clients1"})
db.clients.createIndex({'phone': -1 }, {name: "index_phones"})

20.Usuń drugi indeks z zadania 19.
db.orders.dropIndex("index_phones")