#!usr/bin/env python3
from typing import Optional
from typing import List
def ex1():
    """
    1. Reverse the list without using builtfunctions
    """
    add_number = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19]
    reverse_number = add_number[::-1]
    print(reverse_number)


def ex2():
    """
    2. What's th value of x?
    """
    x = map(lambda x: x * 2, range(5))
    print("x = <map at object")


def ex3():
    """
    3. Get a list of users from the list for users in the USA
    """
    users = [
            {"name": "Bill", "age": 37, "country": "USA"},
            {"name": "Bill1", "age": 21, "country": "USA1"},
            {"name": "Bill2", "age": 17, "country": "USA3"},
            {"name": "Bill3", "age": 27, "country": "USA2"},
    ]
    for user in users:
        for data in user:
            if user[data] == "USA":
                print(user)
    use = [x for x in [user for user in users] if x['country'] == "USA"]
    print(use)


def ex4():
    """
    4. Write a function that will return all numbers that exist in both list
    """
    list_a = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21, 23]
    list_b = [4, 8, 15, 16, 23, 42]
    exists_numb = []
    for listb in list_b:
        if listb in list_a:
            exists_numb.append(listb)

    print(exists_numb)

def ex16():
    class Employees:
        def __init__(self, company, user_hired, hire_data, start_data, 
                status, employment):
            self.company = company
            self.user_hired = user_hired
            self.hire_data = hire_data
            self.start_data = start_data
            self.status = status
            self.employment= employment


ex1()
ex2()
ex3()
ex4()
ex16()

print("5) answer: B")
print("6) answer: C")
print("7) answer: C")
print("8) answer: C")
print("9) answer: C")
print("10) answer: D")
print("12) answer: B")
print("13) answer: B")
print("14) answer: D")
print("15) answer: A")
print("17) answer: B")
